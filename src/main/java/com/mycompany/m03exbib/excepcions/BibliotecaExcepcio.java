
package com.mycompany.m03exbib.excepcions;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mfontana
 */
public class BibliotecaExcepcio extends Exception {
    
    public static final int CODI_REPETIT = 0;
    public static final int CODI_NO_EXISTEIX = 1;
    public static final int BIB_BUIDA = 2;
    public static final int NO_LLIBRES_GENERE = 3;
    
    private final List<String> missatges = Arrays.asList(
            "ERROR: Ja existeix un llibre amb aquest codi",
            "ERROR: No disposem de cap llibre amb aquest codi a la nostra biblioteca",
            "La biblioteca està buida.",
            "ERROR: No disposem de cap llibre del gènere indicat a la nostra biblioteca");
    
    private final int code;

    public BibliotecaExcepcio(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return missatges.get(code);
    }
    
}
