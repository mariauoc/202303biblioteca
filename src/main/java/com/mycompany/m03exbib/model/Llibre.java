
package com.mycompany.m03exbib.model;

/**
 *
 * @author mfontana
 */
public class Llibre {
    private String codi;
    private String titol;
    private String autor;
    private int numPag;
    private String genere;
    private double preu;
    private boolean enPrestec;

    public Llibre(String codi, String titol, String autor, int numPag, String genere, double preu, boolean enPrestec) {
        this.codi = codi;
        this.titol = titol;
        this.autor = autor;
        this.numPag = numPag;
        this.genere = genere;
        this.preu = preu;
        this.enPrestec = enPrestec;
    }

    public String getCodi() {
        return codi;
    }

    public String getGenere() {
        return genere;
    }

    public String getTitol() {
        return titol;
    }

    public int getNumPag() {
        return numPag;
    }

    public boolean isEnPrestec() {
        return enPrestec;
    }

    public double getPreu() {
        return preu;
    }

    @Override
    public String toString() {
        String cadena = "Codi: " + codi + "\n";
        cadena += "Títol: " + titol + "\n";
        cadena += "Autor: " + autor + "\n";
        cadena += "Número de pàgines: " + numPag + "\n";
        cadena += "Gènere: " + genere + "\n";
        cadena += "Preu: " + preu + "€\n";
        cadena += "En prèstec: ";
        cadena += enPrestec?"Sí":"No";
        return cadena;
    }
    
    
    
}
