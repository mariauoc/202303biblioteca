
package com.mycompany.m03exbib.model;

import com.mycompany.m03exbib.excepcions.BibliotecaExcepcio;
import java.util.ArrayList;

/**
 *
 * @author mfontana
 */
public class Biblioteca {
    private ArrayList<Llibre> llibres;

    public Biblioteca() {
        llibres = new ArrayList<>();
    }
    
    public Llibre obtenirLlibrePerCodi(String codi) throws BibliotecaExcepcio {
        for (Llibre l : llibres) {
            if (l.getCodi().equalsIgnoreCase(codi)) {
                return l;
            }
        }
        return null;
    }
    
    public boolean esBuida() {
        return llibres.isEmpty();
    }
    
    public void afegirLlibre(Llibre l) {
        llibres.add(l);
    }
    
    public ArrayList<Llibre> llibresPerGenere(String genere) throws BibliotecaExcepcio {
         if (llibres.isEmpty()) {
             throw new BibliotecaExcepcio(BibliotecaExcepcio.BIB_BUIDA);
        }
        ArrayList<Llibre> llibresGenere = new ArrayList<>();
        for (Llibre l : llibres) {
            if (l.getGenere().equalsIgnoreCase(genere)) {
                llibresGenere.add(l);
            }
        }
        return llibresGenere;
    }
    
    public Llibre llibreMax() throws BibliotecaExcepcio {
         if (llibres.isEmpty()) {
             throw new BibliotecaExcepcio(BibliotecaExcepcio.BIB_BUIDA);
        }
        Llibre llibreMax = llibres.get(0);
        for (Llibre l : llibres) {
            if (l.getNumPag() > llibreMax.getNumPag()) {
                llibreMax = l;
            }
        }
        return llibreMax;
    }
    
    public int totalLlibres() {
        return llibres.size();
    }
    
    public int numLlibresEnPrestec() {
        int total = 0;
        for (Llibre l : llibres) {
            if (l.isEnPrestec()) {
                total++;
            }
        }
        return total;
    }
    
    public double preuTotal() {
        double total = 0;
        for (Llibre l : llibres) {
            total += l.getPreu();
        }
        return total;
    }
    
}
