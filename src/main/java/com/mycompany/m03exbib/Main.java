package com.mycompany.m03exbib;

import com.mycompany.m03exbib.model.Biblioteca;
import com.mycompany.m03exbib.model.Llibre;
import com.mycompany.m03exbib.excepcions.BibliotecaExcepcio;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author mfontana
 */
public class Main {

    private static Biblioteca laBiblioteca;


    public static void main(String[] args) throws IOException {
        laBiblioteca = new Biblioteca();
        int opcio = 0;
        do {
            try {
                mostrarMenu();
                opcio = DemanarDades.demanarEnter("Indica una opció: ");
                switch (opcio) {
                    case 1:
                        nouLlibre();
                        break;
                    case 2:
                        consultarLlibre();
                        break;
                    case 3:
                        llibresPerGenere();
                        break;
                    case 4:
                        llibreMesGros();
                        break;
                    case 5:
                        totals();
                        break;
                    case 6:
                        System.out.println("Fins aviat!");
                        break;
//                    case 7: // caso para pruebas
//                        dadesProves();
//                        break;
                    default:
                        System.out.println("Opció incorrecta.");
                }
            } catch (BibliotecaExcepcio ex) {
                System.out.println(ex.getMessage());
            }
        } while (opcio != 6);
    }
    
    private static void totals() {
        System.out.println("Llibres en prèstec: " + laBiblioteca.numLlibresEnPrestec());
        System.out.println("Número de llibres: " + laBiblioteca.totalLlibres());
        System.out.println("Valor de la biblioteca: " + laBiblioteca.preuTotal() + "€");
    }
    
    private static void llibreMesGros() throws BibliotecaExcepcio {
        Llibre max = laBiblioteca.llibreMax();
        System.out.println("Llibre més gros de la biblioteca");
        System.out.println("Títol: " + max.getTitol());
        System.out.println("Número de pàgines: " + max.getNumPag());
        
    }
    private static void llibresPerGenere() throws BibliotecaExcepcio, IOException {
        String genere = DemanarDades.demanarCadenaNoBuida("Gènere que vols consultar:");
        ArrayList<Llibre> llibresGenere = laBiblioteca.llibresPerGenere(genere);
        if (llibresGenere.isEmpty()) {
            throw new BibliotecaExcepcio(BibliotecaExcepcio.NO_LLIBRES_GENERE);
        }
        System.out.println("Llibres del gènere " + genere);
        for (Llibre l : llibresGenere) {
            System.out.println(l.getTitol());
        }
    }

    private static void consultarLlibre() throws IOException, BibliotecaExcepcio {
        String codi = DemanarDades.demanarCadenaNoBuida("Codi del llibre:");
        Llibre l = laBiblioteca.obtenirLlibrePerCodi(codi);
        if (l == null) {
            throw new BibliotecaExcepcio(BibliotecaExcepcio.CODI_NO_EXISTEIX);
        }
        System.out.println("Dades del llibre:");
        System.out.println(l);
    }

    private static void nouLlibre() throws IOException, BibliotecaExcepcio {
        String codi = DemanarDades.demanarCadenaNoBuida("Codi del llibre:");
        Llibre l = laBiblioteca.obtenirLlibrePerCodi(codi);
        if (l != null) {
            throw new BibliotecaExcepcio(BibliotecaExcepcio.CODI_REPETIT);
        }
        String titol = DemanarDades.demanarCadenaNoBuida("Títol:");
        String autor = DemanarDades.demanarCadenaNoBuida("Autor:");
        String genere = DemanarDades.demanarCadenaNoBuida("Gènere:");
        int numPag = DemanarDades.demanarEnter("Número de pàgines:", 0);
        double preu = DemanarDades.demanarDoble("Preu:", -1);
        boolean enPrestec = DemanarDades.demanarBoolean("El llibre està en prèstec?(s/n)", "s", "n");
        laBiblioteca.afegirLlibre(new Llibre(codi, titol, autor, numPag, genere, preu, enPrestec));
        System.out.println("Llibre registrat.");
    }

    private static void mostrarMenu() {
        System.out.println("**** Gestió de la biblioteca ***");
        System.out.println("1. Nou llibre");
        System.out.println("2. Consultar dades d'un llibre");
        System.out.println("3. Llistat de llibres d'un gènere");
        System.out.println("4. Llibre més gros");
        System.out.println("5. Totals");
        System.out.println("6. Sortir");
    }

    private static void dadesProves() {
        laBiblioteca.afegirLlibre(new Llibre("001", "La historia Interminable", "Michael Ende", 420, "Fantasia", 15.15, false));
        laBiblioteca.afegirLlibre(new Llibre("002", "El principito", "Antoine de Saint-Exupéry", 120, "Infantil", 6.60, true));
        laBiblioteca.afegirLlibre(new Llibre("003", "Nacidos de la bruma: El imperio final", "Brandon Sanderson", 541, "fantasia", 19.85, true));
        laBiblioteca.afegirLlibre(new Llibre("004", "El camino de los reyes", "Brandon Sanderson", 1200, "fantasia", 33.15, false));
    }

}
